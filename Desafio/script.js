var world = "Hello World"

document.querySelector("#first").innerHTML = world;
document.querySelector("#second").innerHTML = world.split("").reverse().join("")
document.querySelector("#third").innerHTML = world.padStart(77, "&nbsp;")
document.querySelector("#fourth").innerHTML = world.toLowerCase()
document.querySelector("#fifth").innerHTML = world.toUpperCase()
document.querySelector("#sixth").innerHTML = byLine(world)
document.querySelector("#seventh").innerHTML = perInvertedLine(world)
document.querySelector("#eighth").innerHTML = world.bold()
document.querySelector("#ninth").innerHTML = spaceBetween(world)
document.querySelector("#tenth").innerHTML = world.split(" ").reverse().join();

function byLine(world) {
  var formatted = ""

  for (var i = 0; i < world.length; i++) {

    formatted += world.charAt(i) + "<br>"

  }
  return formatted
}

function perInvertedLine(world) {
  var formatted = ""

  for (var i = world.length - 1; i >= 0; i--) {

    formatted += world.charAt(i) + "<br>"

  }
  return formatted
}

function spaceBetween(world) {
  var formatted = ""

  for (var i = 0; i < world.length; i++) {
    formatted += world.charAt(i) + "&nbsp;"
  }

  return formatted
}